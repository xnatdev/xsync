package org.nrg.xsync.utils;

public class DefaultZipFileGenerator extends ZipFileGeneratorA {

    public DefaultZipFileGenerator(final long maxUncompressedFileSize) {
        super(maxUncompressedFileSize);
    }
}
