package org.nrg.xnat.xsync.transformer;

/**
 * @author Mohana Ramaratnam (mohanakannan9@gmail.com)
 * 
 * Helper class to provide all the attributes
 *
 */
public class TransformerHelper {
	public static final String LOCAL_PROJECT_ID = "local_project_id";
	public static final String REMOTE_PROJECT_ID = "remote_project_id";
	
	public static final String LOCAL_SUBJECT_ID = "local_subject_id";
	public static final String LOCAL_SUBJECT_LABEL = "local_subject_label";
	public static final String REMOTE_SUBJECT_ID = "remote_subject_id";
	public static final String REMOTE_SUBJECT_LABEL = "remote_subject_label";
	
	public static final String LOCAL_EXP_ID = "local_exp_id";
	public static final String LOCAL_EXP_LABEL = "local_exp_label";
	public static final String REMOTE_EXP_ID = "remote_exp_id";
	public static final String REMOTE_EXP_LABEL = "remote_exp_label";

	public static final String LOCAL_IMAGEASSESSOR_ID = "local_iamgeassessor_id";
	public static final String LOCAL_IMAGEASSESSOR_lABEL = "local_imageassessor_label";
	public static final String REMOTE_IMAGEASSESSOR_ID = "remote_imageassessor_id";
	public static final String REMOTE_IMAGEASSESSOR_LABEL = "remote_imageassessor_label";

	public static final String LOCAL_IMAGESESSION_ID = "local_imagesession_id";
	public static final String LOCAL_IMAGESESSION_lABEL = "local_imagesession_label";
	public static final String REMOTE_IMAGESESSION_ID = "remote_imagesession_id";
	public static final String REMOTE_IMAGESESSION_LABEL = "remote_imagesession_label";

}
